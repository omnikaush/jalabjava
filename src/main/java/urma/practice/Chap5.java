package urma.practice;



import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

/**
 * Created by Adam on 7/22/2015.
 */
public class Chap5 {

    public static void main(String[] args) {


        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario","Milan");
        Trader alan = new Trader("Alan","Cambridge");
        Trader brian = new Trader("Brian","Cambridge");

        List<Transaction> transactions = Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)
        );





        // Question 1: Find all transactions from year 2011 and sort them by value (small to high).


        // Question 2: What are all the unique cities where the traders work?


        // Question 3: Find all traders from Cambridge and sort them by name.


        // Question 4: Return a string of all traders’ names sorted alphabetically.


        // Question 5: Are there any trader based in Milan?


        // Question 6: Update all transactions so that the traders from Milan are set to Cambridge.


        // Question 7: What's the highest value in all the transactions?
 





    }
}
